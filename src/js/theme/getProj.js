/* eslint-disable */
class Proj {
    constructor() {
        this.proj = '';

        this.nodes = {
            'proj_item': '.prj-item',
            'work_link': '.work-link',
            'proj_popup': '#proj-wrapper',
            'proj_popup_item': '.proj-show-item',
            'planet': '.planet',

        }
    }
    get isOpen() {
        return this.searchIsOpen;
    }

    toggleOpen() {
        this.searchIsOpen = !this.searchIsOpen;
    }
    requestSearch(s) {

        if (this.xhr && this.xhr.readyState != 4) {
            this.xhr.abort();
        }

        if (s === '') {
            closeSearch();
            return false;
        };


        this.projID = s;

        this.xhr = new XMLHttpRequest();
        this.xhr.open('POST', settings.ajaxurl, true);
        this.xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        this.xhr.onload = () => {

            if (this.xhr.status != 200 && this.status < 400) {
                console.error(`Error ${this.xhr.status}: ${this.xhr.statusText}`);
            } else {
                if (`${this.xhr.response}` == '') {
                    console.error('no response');
                    return false;
                }

                let r = JSON.parse(`${this.xhr.response}`);


                goPlanet();
                setTimeout(() => {
                    this.buildSearchResults(r);
                }, 2500);
            }
        };

        this.xhr.onloadstart = function(e) {};

        this.xhr.send(`action=getProj&proj_id=${this.projID}`);
    }

    buildSearchResults(r) {
        console.log(r)
        const proj_name = `<p>${r.name}</p>`;

        document.querySelector(this.nodes.proj_popup_item + ' .proj-title').innerHTML = r.name;
        document.querySelector(this.nodes.proj_popup_item + ' .proj-client').innerHTML = r.client;
        document.querySelector(this.nodes.proj_popup_item + ' .description').innerHTML = r.description;
        document.querySelector(this.nodes.proj_popup_item + ' .proj_img').setAttribute('src', r.img);
        document.querySelector(this.nodes.proj_popup_item + ' .proj-url').setAttribute('href', r.url);


        setTimeout(() => {
            document.querySelector('.planet-wrapper').classList.remove('show');
            document.querySelector('.planet-wrapper').classList.add('hide');
            document.querySelector('.planet-wrapper').classList.add('d-none');
            document.querySelector(this.nodes.proj_popup_item).classList.remove('d-none');
            document.querySelector(this.nodes.proj_popup_item).classList.add('show');
        }, 500);
    }
    clearResults() {
        document.querySelector(this.nodes.proj_popup_item + ' .proj-title').innerHTML = '';
        document.querySelector(this.nodes.proj_popup_item + ' .proj-client').innerHTML = '';
        document.querySelector(this.nodes.proj_popup_item + ' .description').innerHTML = '';
        document.querySelector(this.nodes.proj_popup_item + ' .proj_img').setAttribute('src', '');
        document.querySelector(this.nodes.proj_popup_item + ' .proj-url').setAttribute('href', '');
    }

}

var s;

const goPlanet = () => {
    var wrapper = document.querySelector('#proj-wrapper');
    document.querySelector('.planet').innerHTML = '';
    wrapper.classList.remove('d-none');
    fullpage_api.setAllowScrolling(false);

    for (var j = 0; j < 300; j++) {
        var element = document.createElement('div');
        element.setAttribute('class', 'particle-planet');
        document.querySelector('.planet').appendChild(element);
    }

    wrapper.classList.remove('hide');
    wrapper.classList.add('show');
    setTimeout(() => {
        document.querySelector('.proj-show-item').classList.remove('hide');
        document.querySelector('.proj-show-item').classList.add('show');
        document.querySelector('.planet-wrapper').classList.remove('d-none');
        document.querySelector('.planet-wrapper').classList.remove('hide');
        document.querySelector('.planet-wrapper').classList.add('show');
    }, 500);
}
const closePlanet = () => {
    var wrapper = document.querySelector('#proj-wrapper');
    setTimeout(() => {
        document.querySelector('.proj-show-item').classList.remove('show');
        document.querySelector('.proj-show-item').classList.add('hide');
    }, 100);
    setTimeout(() => {
        wrapper.classList.add('hide');
        wrapper.classList.remove('show');
    }, 200);
    setTimeout(() => {
        wrapper.classList.add('d-none');
        document.querySelector('.planet').innerHTML = '';

        fullpage_api.setAllowScrolling(true);
    }, 700);

    for (var j = 0; j < 300; j++) {
        var element = document.createElement('div');
        element.setAttribute('class', 'particle-planet');
        document.querySelector('.planet').appendChild(element);
    }


}

(function(d, w, $) {
    s = new Proj();

    var work_links = document.getElementsByClassName('work-link');

    for (var i = 0; i < work_links.length; i++) {
        work_links[i].addEventListener('click', e => {
            e.preventDefault();

            s.requestSearch(e.currentTarget.getAttribute('data-target'));
        });
    }

    document.querySelector('.close-proj').addEventListener('click', () => {
        closePlanet();
        s.clearResults();
    })
})(document, window, jQuery);