window.eventGA = (ele, args) => {
	if (typeof ele === 'undefined' || typeof ga !== 'function' || window.settings.wpenv !== 'production') {
		return false;
	}

	window.ga('send', 'event', {
		eventCategory: args ? args.category : ele.dataset.category,
		eventAction: args ? args.action : ele.dataset.action,
		eventLabel: args ? args.label : ele.dataset.label
	});

};
