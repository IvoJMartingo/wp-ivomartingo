/* eslint-disable */

function makeDiv(i) {
    // vary size for fun
    var divsize = ((Math.random() * 200) + 50).toFixed();
    var divsizeW = $('.home-banner-dots .position-relative').width();
    var divsizeH = $('.home-banner-dots .position-relative').height();
    //var color = '#' + Math.round(0xffffff * Math.random()).toString(16);
    var newdiv = $('<div/>').css({
        'width': divsize + 'px',
        'height': divsize + 'px'
    });

    // make position sensitive to size and document's width
    var posx = (Math.random() * (divsizeW - divsize)).toFixed();
    var posy = (Math.random() * (divsizeH - divsize)).toFixed();

    newdiv.css({
        'position': 'absolute',
        'left': posx + 'px',
        'top': posy + 'px',
        'display': 'none'
    }).appendTo('.home-banner-dots .position-relative').fadeIn(1000 * i).delay(3000).fadeOut(1000 * i, function() {
        $(this).remove();
        makeDiv(i);
    });
};

for (var i = 1; i <= 3; i++) {
    makeDiv(i);
};

new fullpage('#main', {
    licenseKey: 'A5DFDF99-2A01459E-86E87FF8-E2CF12FA',
    anchors: ['home', 'portfolio', 'about', 'contact'],
    menu: '#main-menu',
    //options here
    onLeave: function(origin, destination, direction) {
        var params = {
            origin: origin,
            destination: destination,
            direction: direction
        };

        if (Object.values(origin)[0] == 'second-section') {
            document.querySelector('#portfolio-bg').style.backgroundImage = 'url()';
            document.querySelector('.website-madeat h5').innerHTML = '';
        }
    },
    afterLoad: function(origin, destination, direction) {
        var params = {
            origin: origin,
            destination: destination,
            direction: direction
        };
        /*  if (isMobile == false) {
             if (Object.values(destination)[0] == 'contact' && Object.values(direction)[0] == 'd') {
                 document.querySelector('.navbar-toggler').classList.add('on-view');
                 document.querySelector('.navbar-collapse').classList.add('on-view');
             } else {
                 document.querySelector('.navbar-collapse').classList.remove('on-view');
                 document.querySelector('.navbar-toggler').classList.remove('on-view');
             }
         } */
    }
});
fullpage_api.setRecordHistory(false);


/*********** ABOUT PARTICLES *************/

if (WEBGL.isWebGLAvailable() === false) {
    document.body.appendChild(WEBGL.getWebGLErrorMessage());
}

var SEPARATION = 100,
    AMOUNTX = 50,
    AMOUNTY = 50;

var container, stats;
var camera, scene, renderer;

var particles, count = 0;

var mouseX = 0,
    mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

init();
animate();

function init() {

    container = document.createElement('div');
    container.setAttribute('id', 'particle_bg');
    document.querySelector('#section0').appendChild(container);

    camera = new THREE.PerspectiveCamera(100, window.innerWidth / (window.innerHeight / 2), 0.1, 5000);
    camera.position.z = 3500;

    scene = new THREE.Scene();

    //

    var numParticles = AMOUNTX * AMOUNTY;

    var positions = new Float32Array(numParticles * 3);
    var scales = new Float32Array(numParticles);

    var i = 0,
        j = 0;

    for (var ix = 0; ix < AMOUNTX; ix++) {
        for (var iy = 0; iy < AMOUNTY; iy++) {
            positions[i] = ix * SEPARATION - ((AMOUNTX * SEPARATION) / 2); // x
            positions[i + 1] = iy * SEPARATION - ((AMOUNTY * SEPARATION) / 4);
            positions[i + 2] = iy * SEPARATION - ((AMOUNTY * SEPARATION) / 2); // z
            scales[j] = 3;
            i += 3;
            j++;
        }
    }

    var geometry = new THREE.BufferGeometry();
    geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));
    geometry.addAttribute('scale', new THREE.BufferAttribute(scales, 1));

    var material = new THREE.ShaderMaterial({

        uniforms: {
            color: { value: new THREE.Color(0xffffff) },
        },
        vertexShader: document.getElementById('vertexshader').textContent,
        fragmentShader: document.getElementById('fragmentshader').textContent

    });

    //

    particles = new THREE.Points(geometry, material);
    scene.add(particles);

    //

    scene.background = new THREE.Color(0x131313)

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    /* stats = new Stats();
    container.appendChild(stats.dom); */

    //document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('touchstart', onDocumentTouchStart, false);
    document.addEventListener('touchmove', onDocumentTouchMove, false);

    //

    window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

//

function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
}

function onDocumentTouchStart(event) {
    if (event.touches.length === 1) {
        event.preventDefault();
        mouseX = event.touches[0].pageX - windowHalfX;
        mouseY = event.touches[0].pageY - windowHalfY;
    }
}

function onDocumentTouchMove(event) {
    if (event.touches.length === 1) {
        event.preventDefault();
        mouseX = event.touches[0].pageX - windowHalfX;
        mouseY = event.touches[0].pageY - windowHalfY;
    }
}

//

function animate() {
    requestAnimationFrame(animate);
    render();
    //stats.update();
}

function render() {

    // camera.position.x += .5;
    // camera.position.y += (-mouseY - camera.position.y) * .05;
    camera.lookAt(scene.position);

    var positions = particles.geometry.attributes.position.array;
    var scales = particles.geometry.attributes.scale.array;

    var i = 0,
        j = 0;

    for (var ix = 0; ix < AMOUNTX; ix++) {

        for (var iy = 0; iy < AMOUNTY; iy++) {
            positions[i + 1] = (Math.sin((ix + count) * 0.3) * 100) +
                (Math.sin((iy + count) * 1) * 100);

            scales[j] = (Math.sin((ix + count) * 1) + 1) * 10 +
                (Math.sin((iy + count) * 0.75) + 1) * 100;

            i += 3;
            j++;
        }
    }

    particles.geometry.attributes.position.needsUpdate = true;
    particles.geometry.attributes.scale.needsUpdate = true;

    renderer.render(scene, camera);

    count += 0.01;

}



jQuery(document).ready(function() {
    var scroller = new ScrollMagic.Controller();
    var animated_ele = document.querySelectorAll('.animate');

    jQuery('.animate').each(function() {

        new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.9,
                reverse: false
            })
            .setClassToggle(this, 'animated')
            //.addIndicators()
            .addTo(scroller);
    });
});