"use strict";

/* eslint-disable */
function makeDiv(i) {
  // vary size for fun
  var divsize = (Math.random() * 200 + 50).toFixed();
  var divsizeW = $('.home-banner-dots .position-relative').width();
  var divsizeH = $('.home-banner-dots .position-relative').height(); //var color = '#' + Math.round(0xffffff * Math.random()).toString(16);

  var newdiv = $('<div/>').css({
    'width': divsize + 'px',
    'height': divsize + 'px'
  }); // make position sensitive to size and document's width

  var posx = (Math.random() * (divsizeW - divsize)).toFixed();
  var posy = (Math.random() * (divsizeH - divsize)).toFixed();
  newdiv.css({
    'position': 'absolute',
    'left': posx + 'px',
    'top': posy + 'px',
    'display': 'none'
  }).appendTo('.home-banner-dots .position-relative').fadeIn(1000 * i).delay(3000).fadeOut(1000 * i, function () {
    $(this).remove();
    makeDiv(i);
  });
}

;

for (var i = 1; i <= 3; i++) {
  makeDiv(i);
}

;
new fullpage('#main', {
  licenseKey: 'A5DFDF99-2A01459E-86E87FF8-E2CF12FA',
  anchors: ['home', 'portfolio', 'about', 'contact'],
  menu: '#main-menu',
  //options here
  onLeave: function onLeave(origin, destination, direction) {
    var params = {
      origin: origin,
      destination: destination,
      direction: direction
    };

    if (Object.values(origin)[0] == 'second-section') {
      document.querySelector('#portfolio-bg').style.backgroundImage = 'url()';
      document.querySelector('.website-madeat h5').innerHTML = '';
    }
  },
  afterLoad: function afterLoad(origin, destination, direction) {
    var params = {
      origin: origin,
      destination: destination,
      direction: direction
    };
    /*  if (isMobile == false) {
         if (Object.values(destination)[0] == 'contact' && Object.values(direction)[0] == 'd') {
             document.querySelector('.navbar-toggler').classList.add('on-view');
             document.querySelector('.navbar-collapse').classList.add('on-view');
         } else {
             document.querySelector('.navbar-collapse').classList.remove('on-view');
             document.querySelector('.navbar-toggler').classList.remove('on-view');
         }
     } */
  }
});
fullpage_api.setRecordHistory(false);
/*********** ABOUT PARTICLES *************/

if (WEBGL.isWebGLAvailable() === false) {
  document.body.appendChild(WEBGL.getWebGLErrorMessage());
}

var SEPARATION = 100,
    AMOUNTX = 50,
    AMOUNTY = 50;
var container, stats;
var camera, scene, renderer;
var particles,
    count = 0;
var mouseX = 0,
    mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
init();
animate();

function init() {
  container = document.createElement('div');
  container.setAttribute('id', 'particle_bg');
  document.querySelector('#section0').appendChild(container);
  camera = new THREE.PerspectiveCamera(100, window.innerWidth / (window.innerHeight / 2), 0.1, 5000);
  camera.position.z = 3500;
  scene = new THREE.Scene(); //

  var numParticles = AMOUNTX * AMOUNTY;
  var positions = new Float32Array(numParticles * 3);
  var scales = new Float32Array(numParticles);
  var i = 0,
      j = 0;

  for (var ix = 0; ix < AMOUNTX; ix++) {
    for (var iy = 0; iy < AMOUNTY; iy++) {
      positions[i] = ix * SEPARATION - AMOUNTX * SEPARATION / 2; // x

      positions[i + 1] = iy * SEPARATION - AMOUNTY * SEPARATION / 4;
      positions[i + 2] = iy * SEPARATION - AMOUNTY * SEPARATION / 2; // z

      scales[j] = 3;
      i += 3;
      j++;
    }
  }

  var geometry = new THREE.BufferGeometry();
  geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));
  geometry.addAttribute('scale', new THREE.BufferAttribute(scales, 1));
  var material = new THREE.ShaderMaterial({
    uniforms: {
      color: {
        value: new THREE.Color(0xffffff)
      }
    },
    vertexShader: document.getElementById('vertexshader').textContent,
    fragmentShader: document.getElementById('fragmentshader').textContent
  }); //

  particles = new THREE.Points(geometry, material);
  scene.add(particles); //

  scene.background = new THREE.Color(0x131313);
  renderer = new THREE.WebGLRenderer({
    antialias: true
  });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  container.appendChild(renderer.domElement);
  /* stats = new Stats();
  container.appendChild(stats.dom); */
  //document.addEventListener('mousemove', onDocumentMouseMove, false);

  document.addEventListener('touchstart', onDocumentTouchStart, false);
  document.addEventListener('touchmove', onDocumentTouchMove, false); //

  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
} //


function onDocumentMouseMove(event) {
  mouseX = event.clientX - windowHalfX;
  mouseY = event.clientY - windowHalfY;
}

function onDocumentTouchStart(event) {
  if (event.touches.length === 1) {
    event.preventDefault();
    mouseX = event.touches[0].pageX - windowHalfX;
    mouseY = event.touches[0].pageY - windowHalfY;
  }
}

function onDocumentTouchMove(event) {
  if (event.touches.length === 1) {
    event.preventDefault();
    mouseX = event.touches[0].pageX - windowHalfX;
    mouseY = event.touches[0].pageY - windowHalfY;
  }
} //


function animate() {
  requestAnimationFrame(animate);
  render(); //stats.update();
}

function render() {
  // camera.position.x += .5;
  // camera.position.y += (-mouseY - camera.position.y) * .05;
  camera.lookAt(scene.position);
  var positions = particles.geometry.attributes.position.array;
  var scales = particles.geometry.attributes.scale.array;
  var i = 0,
      j = 0;

  for (var ix = 0; ix < AMOUNTX; ix++) {
    for (var iy = 0; iy < AMOUNTY; iy++) {
      positions[i + 1] = Math.sin((ix + count) * 0.3) * 100 + Math.sin((iy + count) * 1) * 100;
      scales[j] = (Math.sin((ix + count) * 1) + 1) * 10 + (Math.sin((iy + count) * 0.75) + 1) * 100;
      i += 3;
      j++;
    }
  }

  particles.geometry.attributes.position.needsUpdate = true;
  particles.geometry.attributes.scale.needsUpdate = true;
  renderer.render(scene, camera);
  count += 0.01;
}

jQuery(document).ready(function () {
  var scroller = new ScrollMagic.Controller();
  var animated_ele = document.querySelectorAll('.animate');
  jQuery('.animate').each(function () {
    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 0.9,
      reverse: false
    }).setClassToggle(this, 'animated') //.addIndicators()
    .addTo(scroller);
  });
});
"use strict";

window.eventGA = function (ele, args) {
  if (typeof ele === 'undefined' || typeof ga !== 'function' || window.settings.wpenv !== 'production') {
    return false;
  }

  window.ga('send', 'event', {
    eventCategory: args ? args.category : ele.dataset.category,
    eventAction: args ? args.action : ele.dataset.action,
    eventLabel: args ? args.label : ele.dataset.label
  });
};
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* eslint-disable */
var Proj = /*#__PURE__*/function () {
  function Proj() {
    _classCallCheck(this, Proj);

    this.proj = '';
    this.nodes = {
      'proj_item': '.prj-item',
      'work_link': '.work-link',
      'proj_popup': '#proj-wrapper',
      'proj_popup_item': '.proj-show-item',
      'planet': '.planet'
    };
  }

  _createClass(Proj, [{
    key: "toggleOpen",
    value: function toggleOpen() {
      this.searchIsOpen = !this.searchIsOpen;
    }
  }, {
    key: "requestSearch",
    value: function requestSearch(s) {
      var _this = this;

      if (this.xhr && this.xhr.readyState != 4) {
        this.xhr.abort();
      }

      if (s === '') {
        closeSearch();
        return false;
      }

      ;
      this.projID = s;
      this.xhr = new XMLHttpRequest();
      this.xhr.open('POST', settings.ajaxurl, true);
      this.xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

      this.xhr.onload = function () {
        if (_this.xhr.status != 200 && _this.status < 400) {
          console.error("Error ".concat(_this.xhr.status, ": ").concat(_this.xhr.statusText));
        } else {
          if ("".concat(_this.xhr.response) == '') {
            console.error('no response');
            return false;
          }

          var r = JSON.parse("".concat(_this.xhr.response));
          goPlanet();
          setTimeout(function () {
            _this.buildSearchResults(r);
          }, 2500);
        }
      };

      this.xhr.onloadstart = function (e) {};

      this.xhr.send("action=getProj&proj_id=".concat(this.projID));
    }
  }, {
    key: "buildSearchResults",
    value: function buildSearchResults(r) {
      var _this2 = this;

      console.log(r);
      var proj_name = "<p>".concat(r.name, "</p>");
      document.querySelector(this.nodes.proj_popup_item + ' .proj-title').innerHTML = r.name;
      document.querySelector(this.nodes.proj_popup_item + ' .proj-client').innerHTML = r.client;
      document.querySelector(this.nodes.proj_popup_item + ' .description').innerHTML = r.description;
      document.querySelector(this.nodes.proj_popup_item + ' .proj_img').setAttribute('src', r.img);
      document.querySelector(this.nodes.proj_popup_item + ' .proj-url').setAttribute('href', r.url);
      setTimeout(function () {
        document.querySelector('.planet-wrapper').classList.remove('show');
        document.querySelector('.planet-wrapper').classList.add('hide');
        document.querySelector('.planet-wrapper').classList.add('d-none');
        document.querySelector(_this2.nodes.proj_popup_item).classList.remove('d-none');
        document.querySelector(_this2.nodes.proj_popup_item).classList.add('show');
      }, 500);
    }
  }, {
    key: "clearResults",
    value: function clearResults() {
      document.querySelector(this.nodes.proj_popup_item + ' .proj-title').innerHTML = '';
      document.querySelector(this.nodes.proj_popup_item + ' .proj-client').innerHTML = '';
      document.querySelector(this.nodes.proj_popup_item + ' .description').innerHTML = '';
      document.querySelector(this.nodes.proj_popup_item + ' .proj_img').setAttribute('src', '');
      document.querySelector(this.nodes.proj_popup_item + ' .proj-url').setAttribute('href', '');
    }
  }, {
    key: "isOpen",
    get: function get() {
      return this.searchIsOpen;
    }
  }]);

  return Proj;
}();

var s;

var goPlanet = function goPlanet() {
  var wrapper = document.querySelector('#proj-wrapper');
  document.querySelector('.planet').innerHTML = '';
  wrapper.classList.remove('d-none');
  fullpage_api.setAllowScrolling(false);

  for (var j = 0; j < 300; j++) {
    var element = document.createElement('div');
    element.setAttribute('class', 'particle-planet');
    document.querySelector('.planet').appendChild(element);
  }

  wrapper.classList.remove('hide');
  wrapper.classList.add('show');
  setTimeout(function () {
    document.querySelector('.proj-show-item').classList.remove('hide');
    document.querySelector('.proj-show-item').classList.add('show');
    document.querySelector('.planet-wrapper').classList.remove('d-none');
    document.querySelector('.planet-wrapper').classList.remove('hide');
    document.querySelector('.planet-wrapper').classList.add('show');
  }, 500);
};

var closePlanet = function closePlanet() {
  var wrapper = document.querySelector('#proj-wrapper');
  setTimeout(function () {
    document.querySelector('.proj-show-item').classList.remove('show');
    document.querySelector('.proj-show-item').classList.add('hide');
  }, 100);
  setTimeout(function () {
    wrapper.classList.add('hide');
    wrapper.classList.remove('show');
  }, 200);
  setTimeout(function () {
    wrapper.classList.add('d-none');
    document.querySelector('.planet').innerHTML = '';
    fullpage_api.setAllowScrolling(true);
  }, 700);

  for (var j = 0; j < 300; j++) {
    var element = document.createElement('div');
    element.setAttribute('class', 'particle-planet');
    document.querySelector('.planet').appendChild(element);
  }
};

(function (d, w, $) {
  s = new Proj();
  var work_links = document.getElementsByClassName('work-link');

  for (var i = 0; i < work_links.length; i++) {
    work_links[i].addEventListener('click', function (e) {
      e.preventDefault();
      s.requestSearch(e.currentTarget.getAttribute('data-target'));
    });
  }

  document.querySelector('.close-proj').addEventListener('click', function () {
    closePlanet();
    s.clearResults();
  });
})(document, window, jQuery);
"use strict";

/* eslint-disable */
var isMobile = false; //initiate as false
// device detection

if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
  isMobile = true;
}
/* var animation = bodymovin.loadAnimation({
    container: document.getElementById('lottie'), // Required
    path: settings.dir + '/img/space/Galaxy.json', // Required
    renderer: 'svg/canvas/html', // Required
    loop: true, // Optional
    autoplay: true, // Optional
    name: "Hello World", // Name for future reference. Optional.
}) */


(function (w) {
  bodymovin.loadAnimation({
    container: document.querySelector('#planet'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: w.settings.dir + '/img/space/706-the-final-frontier.json'
  });
  /*    bodymovin.loadAnimation({
         container: document.querySelector('#loader'),
         renderer: 'svg',
         loop: true,
         autoplay: true,
         path: w.settings.dir + '/img/space/8586-rocket-in-space.json'
     }); */
})(window);

window.onload = function (event) {
  setTimeout(function () {
    document.querySelector('#loader').classList.remove('show');
  }, 600);
  setTimeout(function () {
    document.querySelector('#loader').classList.remove('d-flex');
    document.querySelector('#loader').classList.add('d-none');
  }, 850);
};