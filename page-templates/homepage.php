<?php
/**
 * Template Name: Homepage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>


<div class="row mx-0">

	<div class="col-md-12 content-area px-0" id="primary">

		<main class="site-main" id="main" role="main">
			<?php 
			get_template_part( 'page-templates/homepage/part','banner' ); 
			get_template_part( 'page-templates/homepage/part','portfolio' ); 
			get_template_part( 'page-templates/homepage/part','about' ); 
			get_template_part( 'page-templates/homepage/part','contact' ); 
			?>
		</main><!-- #main -->
		<?php get_template_part( 'page-templates/homepage/proj', 'popup' ); ?>

	</div><!-- #primary -->

</div><!-- .row end -->


<?php get_footer(); ?>

