<?php
/**
 * Homepage portfolio partial template.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<script type="x-shader/x-vertex" id="vertexshader">
    attribute float scale;
    void main() {
        vec4 mvPosition = modelViewMatrix * vec4( position, 0.75 );
        gl_PointSize = scale * ( 10.0 / - mvPosition.z );
        gl_Position = projectionMatrix * mvPosition;

    }
</script>
<script type="x-shader/x-fragment" id="fragmentshader">
    uniform vec3 color;
    void main() {
        if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;
        gl_FragColor = vec4( color, 1.0 );
    }
</script>
        
<section id="section3" class="section contact-section position-relative">
    <!-- <div class="circle circle-small"></div> -->
    <div class="say-hello font-trueno-ultra-black text-white animate animate_text">
        <div>
            Say hello !
        </div>
    </div>
    <div class="circle email text-center animate_o_fade animate delay-1000"><a href="mailto:hello@ivomartingo.pt"><?php echo file_get_contents( get_template_directory() . '/img/svg/email.svg' ); ?></a></div>
    <div class="circle linkedin text-center d-flex d-md-none"><a target="_blank" class="d-block" href="https://www.linkedin.com/in/ivo-martingo/"><?php echo file_get_contents( get_template_directory() . '/img/svg/linkedin.svg' ); ?></a></div>
    
    <div class="home-banner-dots d-none d-md-block">
        <div class="inner position-relative w-100 h-100"></div>
    </div>
</section>

<script src="<?php echo get_template_directory_uri(); ?>/js/interactive_particles.js"></script>