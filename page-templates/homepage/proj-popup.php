<?php
/**
 * Homepage portfolio partial template.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<div id="proj-wrapper" class="d-none row proj-wrapper mx-0">
    <div class="planet-wrapper d-none hide">
        <div class="planet"></div>
    </div>
    <a class="close-proj cursor--pointer d-block">
        <?php echo file_get_contents( get_template_directory() . '/img/svg/close.svg'); ?>
    </a>
    <div class="proj-show-item col-12 d-none py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-1">
                    <h2 class="proj-title font-trueno-extra-bold color--white"></h2>
                    <h5 class="proj-client font-trueno color--white"></h5>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 col-md-4 offset-md-2">
                    <p class="font-trueno-light color--white description"></p>
                </div>
                <div class="col-12 col-md-6 ">
                    <img src="" class="proj_img" alt="Ivo Martingo | Web Developer">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-3 offset-md-2 text-center">
                    <div>
                        <a href="" class="proj-url">Go To</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>