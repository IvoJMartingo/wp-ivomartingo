<?php
/**
 * Homepage portfolio partial template.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<section id="section2" class="section about-section position-relative">
    <div class="circle circle-big d-none"></div>
    <div class="about-rocket" id="rocket"></div>
    <div class="container-fluid px-0 h-100 d-none" id="lottie">
    </div>
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 col-md-8 pt-15 py-md-0 h-100">
                <div class="row h-100 align-items-center align-content-center">
                    <div class="col-12 col-md-12 about-text">
                        <p class="text-white font-trueno-ultra-light text-center">O meu nome é Ivo Martingo e sou do Porto. Comecei a programar no meu estágio da faculdade e desde aí tem sido um vício que não quero largar. Desenvolvo e trabalho em sites de pequena (one page, landing pages, campanhas) e grande dimensão, tal como KIA Portugal.
                        </p>
                    </div>
                    <div class="col-12 mt-5">
                        <div class="row h-100">
                            <div class="col-12 col-md-4 text-center jobs">
                                <p class="font-trueno-light text-white">Junior Front-end Developer</p>
                                <p class="font-trueno-light text-white">Covet Group
                                    <small class="font-trueno-light text-white">junho 2017 - junho 2018</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-4 text-center jobs mt-5 mt-md-0">
                                <p class="font-trueno-light text-white">Web Developer</p>
                                <p class="font-trueno-light text-white">Legendary People + Ideas
                                    <small class="font-trueno-light text-white">setembro 2018 - presente</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 h-100">
                <div id="me" class="position-relative w-100 h-100"></div>
            </div>
        </div>
    </div>
</section>