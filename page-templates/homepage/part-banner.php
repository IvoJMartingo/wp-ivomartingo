<?php
/**
 * Homepage banner partial template.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<section id="section0" class="section banner-section position-relative">
    <div class="home-name">
        <h1 class="font-trueno-ultra-light text-white animate animate_text">
            <div class=" delay-1000">
                Ivo Martingo
            </div>
        </h1>
    </div>
    <div class="home-dev">
        <h1 class="font-trueno-semibold text-white animate animate_text ">
            <div class="delay-1000">
                Web Developer
            </div>
        </h1>
    </div>
</section>