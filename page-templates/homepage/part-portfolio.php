<?php
/**
 * Homepage portfolio partial template.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<section id="section1" class="section portfolio-section position-relative">
    <div id="portfolio-bg" class="container h-100">
        <div class="website-madeat d-none d-md-block"><h5 class="font-trueno-ultra-light text-white"></h5></div>
        <div class="row h-100 align-items-center">
            <div class="col-12 col-md-12 mx-auto py-5">
                <div class="row text-center">
                    <?php 
                        $args = array(
                            'post_status'    => 'publish',
                            'post_type'      => 'portfolio',
                            'order'          => 'ASC',
                            'orderby'        => 'title',
                            'posts_per_page' => '6',
                            'paged'          => '1',
                        );
                        echo args_get_template_part( 'loop-templates', 'loop-projects-home', $args );
                    ?>
                </div>
            </div>
        </div>
        <!-- <div class="row cta-container">
            <a href="#" class="cta bg--white text-center color--black font-trueno-light w-100">
                Projects
            </a>
        </div> -->
    </div>
</section>