<?php

/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


$the_query = new WP_Query( $params );


if ( $the_query->have_posts() ) :

	$i = 2;

	while ( $the_query->have_posts() ) :
        $the_query->the_post();

        global $post;
        $post_slug = $post->post_name;

        ?>
        <div class="col-6 col-md-4 py-4 prj-item">
            <a data-target="<?php echo $post_slug; ?>" class="d-block work-link cursor--pointer" data-madeat="Developed at Legendary">
                <div class="h-100 overflow--hidden">
                    <picture class="d-block animate_in_left animate">
                        <source data-srcset="<?php echo get_field('project_img_mobile')['url']; ?>" media="(min-width: 1921px)">
                        <source data-srcset="<?php echo get_field('project_img_mobile')['sizes']['1536x1536']; ?>" media="(min-width: 1600px)">
                        <source data-srcset="<?php echo get_field('project_img_mobile')['sizes']['large']; ?>" media="(min-width: 1200px)">
                        <source data-srcset="<?php echo get_field('project_img_mobile')['sizes']['medium_large']; ?>" media="(min-width: 375px)">
                        <source data-srcset="<?php echo get_field('project_img_mobile')['sizes']['medium']; ?>" media="(min-width: 300px)">
                        <img class="lazy d-block" data-src="<?php echo get_field('project_img_mobile')['sizes']['medium_large']; ?>" lt="Fladgate Port Cocktails | The Vault">
                    </picture>
                </div>
            </a>
        </div>

    <?php endwhile;

endif;