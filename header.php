<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<meta name="theme-color" content="#000000">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site <?php echo is_page_template( 'page-templates/homepage.php' ) ? 'bg--black' : 'bg-color--mid-white'; ?>" id="page" itemscope itemtype="http://schema.org/WebPage">

	<div id="loader" class="d-flex align-items-center justify-content-center show">
		<svg viewBox="0 0 160 160" width="160" height="160">
			<circle cx="80" cy="80" r="50" fill="#ededed"/>
			<g transform=" matrix(0.866, -0.5, 0.25, 0.433, 80, 80)">
				<path d="M 0,70 A 65,70 0 0,0 65,0 5,5 0 0,1 75,0 75,70 0 0,1 0,70Z" fill="#fff">
				<animateTransform attributeName="transform" type="rotate" from="360 0 0" to="0 0 0" dur="1s" repeatCount="indefinite" />
				</path>
			</g>
			<path d="M 50,0 A 50,50 0 0,0 -50,0Z" transform="matrix(0.866, -0.5, 0.5, 0.866, 80, 80)" fill="#ededed" />
		</svg>
	</div>
	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar">

		<nav class="navbar  navbar-dark " itemscope itemtype="http://schema.org/SiteNavigationElement">

			<div class="container">

				<button class="navbar-toggler px-0" type="button" data-toggle="collapse" data-target="#navbarPrimary" aria-controls="navbarPrimary" aria-expanded="false" aria-label="Toggle navigation">
					<?php echo file_get_contents(get_template_directory() . '/img/svg/menu.svg'); ?>
				</button>

				<!-- The WordPress Menu goes here -->
				<div id="navbarPrimary" class="collapse navbar-collapse ml-3">
					<ul id="main-menu" class="navbar-nav">
						<li class="menu-item  menu-item-home nav-item " data-menuanchor="first-section">
							<a title="Home" href="<?php echo is_page_template( 'page-templates/homepage.php' ) ? '' : get_home_url() . '/'; ?>#home" class="nav-link" >Home</a>
						</li>
						<li class="menu-item  menu-item-home nav-item " data-menuanchor="second-section">
							<a title="Portfolio" href="<?php echo is_page_template( 'page-templates/homepage.php' ) ? '' : get_home_url() . '/'; ?>#portfolio" class="nav-link" >Portfolio</a>
						</li>
						<li class="menu-item  menu-item-home nav-item " data-menuanchor="third-section">
							<a title="Portfolio" href="<?php echo is_page_template( 'page-templates/homepage.php' ) ? '' : get_home_url() . '/'; ?>#about" class="nav-link" >Sobre</a>
						</li>
						<li class="menu-item  menu-item-home nav-item " data-menuanchor="fourth-section">
							<a title="Portfolio" href="<?php echo is_page_template( 'page-templates/homepage.php' ) ? '' : get_home_url() . '/'; ?>#contact" class="nav-link" >Contato</a>
						</li>
					</ul>
				</div>
			</div><!-- .container -->

		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->

	<div class="network d-none d-md-block">
		<ul>
			<li><a href="mailto:hello@ivomartingo.pt"><?php echo file_get_contents( get_template_directory() . '/img/svg/email.svg' ); ?></a></li>
			<li><a href="https://www.linkedin.com/in/ivo-martingo/" target="_blank"><?php echo file_get_contents( get_template_directory() . '/img/svg/linkedin.svg' ); ?></a></li>
		</ul>
	</div>

