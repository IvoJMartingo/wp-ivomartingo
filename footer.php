<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$the_theme = wp_get_theme();

?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.6.10/lottie_html.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/@lottiefiles/lottie-interactivity@latest/dist/lottie-interactivity.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r79/three.min.js"></script>

<?php wp_footer(); ?>
</body>

</html>
