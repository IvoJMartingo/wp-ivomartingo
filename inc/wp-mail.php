<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Example to send wp_mail using a HTML template
 */
function send_feedback( $post, $new_status, $email, $voucher_id ) {

	if ( 'checkin' !== get_post_type( $post ) ) {
		return;
	}

	$headers = array( 'Content-Type: text/html; charset=UTF-8' );

	if ( 'publish' === $new_status ) {

		$query_rei = get_posts(
			array(
				'numberposts' => 1,
				'post_type'   => 'rei',
				'title'       => $email,
			)
		);

		$body = wp_remote_retrieve_body( wp_remote_get( get_template_directory_uri() . '/mail-templates/voucher.html' ) );
		$body = str_replace( '[HEADER-IMG]', get_template_directory_uri() . '/mail-templates/assets/header.png', $body );
		$body = str_replace( '[NAME]', get_field( 'checkin_username', $post->ID ), $body );
		$body = str_replace( '[VOUCHER]', $voucher_id, $body );
		$body = str_replace( '[AWARD]', get_field( 'checkin_award', $post->ID ), $body );
		$body = str_replace( '[NCHECKINS]', get_field( 'rei_ncheckins', $query_rei[0]->ID ), $body );
		$body = str_replace( '[FOOTER-IMG]', get_template_directory_uri() . '/mail-templates/assets/footer.png', $body );

		wp_mail( $email, 'NORTADA - Check-in Aprovado!', $body, $headers );
	}

	return;
}

function notification_email( $voucher_id, $post_ID ) {

	$body    = '<p>Novo check recebido com o voucherID <b>' . $voucher_id . '</b>. Por favor valide o mesmo no <a href="' . home_url() . '/rei-login">BO</a></p><br>';
	$body   .= '<p>Caso já esteja logado no BO, poderá aceder a partir deste link <a href="' . get_admin_url() . 'post.php?post=' . $post_ID . '&action=edit">VOUCHER</a></p>';
	$to      = get_option( 'noti_email' ) ? get_option( 'noti_email' ) : 'manuel.monteiro@fcportuense.pt,diogo.guerner@fcportuense.pt';
	$headers = array( 'Content-Type: text/html; charset=UTF-8' );

	wp_mail( $to, 'Novo Check-in', $body, $headers );

}