<?php
/**
 * Process upload WordPress
 * Source : https://rudrastyh.com/wordpress/how-to-add-images-to-media-library-from-uploaded-files-programmatically.html
 *
 * @package ivomartingo
 */


// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// WordPress environment
//require( dirname(__FILE__) . '/../../../wp-load.php' );

/**
 * Do a upload to wordpress media library and returns the post id;
 *
 * @param string $file          The file to be uploaded
 *
 * @return interger;
 */

function process_upload( $file ) {

	$wordpress_upload_dir = wp_upload_dir();

	$i = 1; // number of tries when the file with the same name is already exists

	$new_file_path = $wordpress_upload_dir['path'] . '/' . $file['name'];
	$new_file_mime = mime_content_type( $file['tmp_name'] );

	if ( empty( $file ) ) {
		return 'File is not selected.';
	}

	if ( $file['error'] ) {
		return $file['error'];
	}

	if ( $file['size'] > wp_max_upload_size() ) {
		return 'It is too large than expected.';
	}

	if ( ! in_array( $new_file_mime, get_allowed_mime_types() ) ) {
		return 'WordPress doesn\'t allow this type of uploads.';
	}

	while ( file_exists( $new_file_path ) ) {
		$i++;
		$new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $file['name'];
	}

	// looks like everything is OK
	if ( move_uploaded_file( $file['tmp_name'], $new_file_path ) ) {

		$upload_id = wp_insert_attachment( array(
			'guid'           => $new_file_path,
			'post_mime_type' => $new_file_mime,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', $file['name'] ),
			'post_content'   => '',
			'post_status'    => 'inherit',
		), $new_file_path );

		// wp_generate_attachment_metadata() won't work if you do not include this file
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate and save the attachment metas into the database
		wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );

		return $upload_id;
	} else {
		return false;
	}

}
