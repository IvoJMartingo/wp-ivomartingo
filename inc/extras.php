<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_filter( 'body_class', 'understrap_body_classes' );

if ( ! function_exists( 'understrap_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function understrap_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'adjust_body_class' );

if ( ! function_exists( 'adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' === $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'change_logo_class' );

if ( ! function_exists( 'change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"', $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */
if ( ! function_exists( 'understrap_post_nav' ) ) {

	function understrap_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
				<nav class="container navigation post-navigation">
					<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'understrap' ); ?></h2>
					<div class="row nav-links justify-content-between">
						<?php

						if ( get_previous_post_link() ) {
							previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'understrap' ) );
						}
						if ( get_next_post_link() ) {
							next_post_link( '<span class="nav-next">%link</span>', _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'understrap' ) );
						}
						?>
					</div><!-- .nav-links -->
				</nav><!-- .navigation -->

		<?php
	}
}

/**
 * Load a component into a template while supplying data.
 *
 * @param string $folder          The foder name for the generic template.
 * @param string $slug            The slug name for the generic template.
 * @param array  $params          An associated array of data that will be extracted into the templates scope.
 * @param bool   $output          Whether to output component or return as string.
 *
 * Usage:
 * $post_type = 'cocktail';
 *
 * $args = array(
 *   'post_status'    => 'publish',
 *   'post_type'      => $post_type,
 *   'orderby'        => 'rand',
 *   'posts_per_page' => -1
 * );
 *
 * args_get_template_part('loop-templates','content-'.$post_type,$args);
 *
 * @return string
 */
function args_get_template_part( $folder = 'loop-templates', $slug, array $params = array(), $output = true ) {
	if ( ! $output ) {
		ob_start();
	}

	$template_file = locate_template( "{$folder}/{$slug}.php", false, false );

	if ( ! $template_file ) {
		if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
			/* translators: %s: search term */
			trigger_error( sprintf( __( 'Error locating %s for inclusion', 'understrap' ), esc_html( $file ) ), E_USER_ERROR ); // phpcs:ignore
		}
	}

	compact( $params, EXTR_SKIP );
	require $template_file;
	if ( ! $output ) {
		return ob_get_clean();
	}
}

/**
 * in_array function to multidimensional arrays - https://stackoverflow.com/questions/4128323/in-array-and-multidimensional-array
 *
 * @param string needle - The string to look for
 * @param array haystack - The array that will be searched
 * @param bool return_key_and_item - to return the key and item or bool
 * @param bool strict - To find strict value
 *
 * @return bool
 * @return array
 */

function in_array_r( $needle, $haystack, $return_key_and_item = false, $strict = false ) {
	foreach ( $haystack as $key => $item ) {
		if ( ( $strict ? $item === $needle : $item === $needle ) || ( is_array( $item ) && in_array_r( $needle, $item, $strict ) ) ) {
			return $return_key_and_item ? array( $key, $item ) : true;
		}
	}

	return false;
}

/**
 * multisort function to multidimensional arrays - http://php.net/manual/en/function.array-multisort.php
 *
 * @param array The array that will be sorted
 * @param array column to the sorted
 *
 * Usage : array_msort($arr1, array('name'=>SORT_DESC, 'cat'=>SORT_ASC));
 *
 * @return array
 */

function array_msort( $array, $cols ) {
	$colarr = array();
	foreach ( $cols as $col => $order ) {
		$colarr[ $col ] = array();
		foreach ( $array as $k => $row ) {
			$colarr[ $col ][ '_' . $k ] = strtolower( $row[ $col ] );
		}
	}
	$eval = 'array_multisort(';
	foreach ( $cols as $col => $order ) {
		$eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
	}
	$eval = substr( $eval, 0, -1 ) . ');';
	eval( $eval ); // phpcs:ignore Squiz.PHP.Eval.Discouraged
	$ret = array();
	foreach ( $colarr as $col => $arr ) {
		foreach ( $arr as $k => $v ) {
			$k = substr( $k, 1 );
			if ( ! isset( $ret[ $k ] ) ) {
				$ret[ $k ] = $array[ $k ];
			}
			$ret[ $k ][ $col ] = $array[ $k ][ $col ];
		}
	}
	return $ret;
}