<?php
/**
 * Custom functions for images.
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'cc_mime_types' ) ) {
	// add_filter( 'upload_mimes', 'cc_mime_types' );
	/**
	 * Add additional file types to WordPress media library
	 */
	function cc_mime_types( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		$mimes['eps'] = 'application/postscript';
		$mimes['psd'] = 'image/vnd.adobe.photoshop';
		return $mimes;
	}
}

/**
 * Add custom images sizes
 */
function custom_bg_sizes() {
	add_image_size( 'bg_extra_large_@2x', 3840, 2160 );
	add_image_size( 'bg_extra_large', 1920, 1080 );

	add_image_size( 'bg_large_@2x', 2880, 1800 );
	add_image_size( 'bg_large', 1440, 900 );

	add_image_size( 'bg_medium_@3x', 3072, 2304 );
	add_image_size( 'bg_medium_@2x', 2048, 1536 );
	add_image_size( 'bg_medium', 1024, 768 );

	add_image_size( 'bg_small_@3x', 1800, 2700, true );
	add_image_size( 'bg_small_@2x', 1200, 1800, true );
	add_image_size( 'bg_small', 600, 900, true );

	add_image_size( 'bg_default_@3x', 960, 1440, true );
	add_image_size( 'bg_default_@2x', 640, 930, true );
	add_image_size( 'bg_default', 320, 480, true );
}

add_action( 'after_setup_theme', 'custom_bg_sizes' );

/**
 * Add custom images sizes continue
 */
function custom_bg_sizes_name( $sizes ) {
	return array_merge(
		$sizes, array(
			'bg_extra_large_@2x' => __( 'Extra large background @2x width' ),
			'bg_extra_large'     => __( 'Extra large background width' ),
			'bg_large_@2x'       => __( 'Large background @2x width' ),
			'bg_large'           => __( 'Large background width' ),
			'bg_medium_@3x'      => __( 'Medium background @3x width' ),
			'bg_medium_@2x'      => __( 'Medium background @2x width' ),
			'bg_medium'          => __( 'Medium background width' ),
			'bg_small_@3x'       => __( 'Small background @3x width' ),
			'bg_small_@2x'       => __( 'Small background @2x width' ),
			'bg_small'           => __( 'Small background width' ),
			'bg_default_@3x'     => __( 'Small background @3x width' ),
			'bg_default_@2x'     => __( 'Small background @2x width' ),
			'bg_default'         => __( 'Small background width' ),
		)
	);
}
add_filter( 'image_size_names_choose', 'custom_bg_sizes_name' );

/**
 * Build a style tag with a trully reponsive background
 *
 * @param array      the image sizes array
 * @param string     the DOM selector
 * @param array|null the mobile image sizes array or null
 *
 * Usage : responsive_bg( $bg['sizes'], '#post-' . $post_id, $bg_mob['sizes'] );
 *
 *
 * NOTE : Only works with acf image field ( return image array )
 *
 * @return string|bool
 */
function responsive_bg( $sizes, $selector, $sizes_mob = null ) {
	if ( empty( $sizes ) || is_null( $sizes )
		|| empty( $selector ) || is_null( $selector ) ) {
		return null;
	}

	$have_mob = ! is_null( $sizes_mob );

	ob_start(); ?>
	<style media="screen">
	<?php if ( has_image_size( 'bg_default' ) ) : ?>
		<?php echo esc_attr( $selector ); ?>
		{
			background-image: url(<?php echo esc_url( $have_mob ? $sizes_mob['bg_default'] : $sizes['bg_default'] ); ?>);
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_default_@2x' ) ) : ?>
		/*media('retina2x')*/
		@media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi), (min-resolution: 2dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
					background-image: url(<?php echo esc_url( $have_mob ? $sizes_mob['bg_default_@2x'] : $sizes['bg_default_@2x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_default_@3x' ) ) : ?>
		/*media('retina3x')*/
		@media (-webkit-min-device-pixel-ratio: 3), (-webkit-min-device-pixel-ratio: 3.6458333333333335), (min-resolution: 350dpi), (min-resolution: 3dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
					background-image: url(<?php echo esc_url( $have_mob ? $sizes_mob['bg_default_@3x'] : $sizes['bg_default_@3x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_small' ) ) : ?>
		/*media('>tablet-portrait')*/
		@media (min-width: 601px)
		{
				<?php echo esc_attr( $selector ); ?>
				{
					background-image: url(<?php echo esc_url( $have_mob ? $sizes_mob['bg_small'] : $sizes['bg_small'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_small_@2x' ) ) : ?>
		/*media('>tablet-portrait','retina2x')*/
		@media (min-width: 601px) and (-webkit-min-device-pixel-ratio: 2), (min-width: 601px) and (min-resolution: 192dpi), (min-width: 601px) and (min-resolution: 2dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $have_mob ? $sizes_mob['bg_small_@2x'] : $sizes['bg_small_@2x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_small_@3x' ) ) : ?>
		/*media('>tablet-portrait','retina3x')*/
		@media (min-width: 601px) and (-webkit-min-device-pixel-ratio: 3), (min-width: 601px) and (-webkit-min-device-pixel-ratio: 3.6458333333333335), (min-width: 601px) and (min-resolution: 350dpi), (min-width: 601px) and (min-resolution: 3dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $have_mob ? $sizes_mob['bg_small_@3x'] : $sizes['bg_small_@3x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_medium' ) ) : ?>
		/*media('>tablet-landscape')*/
		@media (min-width: 901px)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_medium'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_medium_@2x' ) ) : ?>
		/*media('>tablet-landscape','retina2x')*/
		@media (min-width: 901px) and (-webkit-min-device-pixel-ratio: 2), (min-width: 901px) and (min-resolution: 192dpi), (min-width: 901px) and (min-resolution: 2dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_medium_@2x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_medium_@3x' ) ) : ?>
		/*media('>tablet-landscape','retina3x')*/
		@media (min-width: 901px) and (-webkit-min-device-pixel-ratio: 3), (min-width: 901px) and (-webkit-min-device-pixel-ratio: 3.6458333333333335), (min-width: 901px) and (min-resolution: 350dpi), (min-width: 901px) and (min-resolution: 3dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_medium_@3x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_large' ) ) : ?>
		/*media('>desktop')*/
		@media (min-width: 1201px)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_large'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_large_@2x' ) ) : ?>
		/*media('>desktop','retina2x')*/
		@media (min-width: 1201px) and (-webkit-min-device-pixel-ratio: 2), (min-width: 1201px) and (min-resolution: 192dpi), (min-width: 1201px) and (min-resolution: 2dppx)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_large_@2x'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_extra_large' ) ) : ?>
		/*media('>large-desktop')*/
		@media (min-width: 1801px)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_extra_large'] ); ?>);
				}
		}
	<?php endif; ?>
	<?php if ( has_image_size( 'bg_extra_large_@2x' ) ) : ?>
		/*media('>large-desktop','retina2x')*/
		/* @media (min-width: 1801px) and (-webkit-min-device-pixel-ratio: 1.5), (min-width: 1801px) and (min-resolution: 192dpi), (min-width: 1801px) and (min-resolution: 2dppx) */
		@media (min-width: 2560px)
		{
				<?php echo esc_attr( $selector ); ?>
				{
						background-image: url(<?php echo esc_url( $sizes['bg_extra_large_@2x'] ); ?>);
				}
		}
	<?php endif; ?>
	</style>
	<?php
	$output = ob_get_contents();
	ob_end_clean();

	echo $output; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
}
