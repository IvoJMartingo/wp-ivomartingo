<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/* Read https://codex.wordpress.org/Function_Reference/check_ajax_referer */

function getProj() {

	$proj_id  = sanitize_text_field($_POST['proj_id']);

	$args = array(
		'post_status'    => 'publish',
		'post_type'      => 'portfolio',
		'posts_per_page' => '1',
		'name' 			 => $proj_id
	);

	$output = new WP_Query( $args );

	$outputID = $output->post->ID;

	echo wp_json_encode( array(
		'ID' => $outputID,
		'name' => get_field('project_name', $outputID),
		'description' => get_field('project_description', $outputID),
		'img' => get_field('project_img', $outputID)['url'],
		'client' => get_field('project_client', $outputID),
		'url' => get_field('project_url', $outputID),
		'project_source' => get_field('project_source', $outputID),
		'company' => get_field('project_source_company_name', $outputID),
		'contribution' => get_field('project_source_freelance_contribution', $outputID),
	) );

	exit();

}

add_action( 'wp_ajax_getProj', 'getProj' );
add_action( 'wp_ajax_nopriv_getProj', 'getProj' );

function filter_store_genre() {
	check_ajax_referer( 'my-special-string', 'security' );

	$posts_per_page = $_POST['postsPerPage'];
	$post_type      = $_POST['postType'];
	$paged          = $_POST['page'];
	$term           = $_POST['term'];

	$args = array(
		'post_status'    => 'publish',
		'post_type'      => $post_type,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'posts_per_page' => $posts_per_page,
		'paged'          => $paged,
		'tax_query'      => array(
			array(
				'taxonomy' => 'genre',
				'field'    => 'slug',
				'terms'    => $term,
			),
		),
	);

	if ( 'all' === $genre_type ) {
		array_pop( $args ); } //remove 'tax_query' arguments

	ob_start();

	echo args_get_template_part( 'loop-templates', 'content-stores', $args );

	$the_query = new WP_Query( $args );

	$output = array(
		'content'     => ob_get_contents(),
		'maxNumPages' => $the_query->max_num_pages,
	);

	ob_end_clean();

	echo wp_json_encode( $output );

	// echo json_encode($args);
	wp_reset_postdata();

	exit();

}

add_action( 'wp_ajax_filter_store_genre', 'filter_store_genre' );
add_action( 'wp_ajax_nopriv_filter_store_genre', 'filter_store_genre' );

function filter_reasons_type() {
	check_ajax_referer( 'my-special-string', 'security' );

	$posts_per_page = $_POST['posts_per_page'];
	$post_type      = $_POST['post_type'];
	$paged          = $_POST['page'];
	$term           = json_decode( stripslashes( $_POST['term'] ) );

	$args = array(
		'post_status'    => 'publish',
		'post_type'      => $post_type,
		'posts_per_page' => $posts_per_page,
		'paged'          => $paged,
		'tax_query'      => array(
			array(
				'taxonomy' => 'reasons_type',
				'field'    => 'term_id',
				'terms'    => $term,
			),
		),
	);

	if ( count( $term ) < 1 ) {
		array_pop( $args ); } //remove 'tax_query' arguments

	ob_start();

	echo args_get_template_part( 'loop-templates', 'content-reason', $args );

	$the_query = new WP_Query( $args );

	$output = array(
		'content'       => ob_get_contents(),
		'max_num_pages' => $the_query->max_num_pages,
	);

	ob_end_clean();

	echo wp_json_encode( $output );

	wp_reset_postdata();

	exit();

}

add_action( 'wp_ajax_filter_reasons_type', 'filter_reasons_type' );
add_action( 'wp_ajax_nopriv_filter_reasons_type', 'filter_reasons_type' );


function live_search() {
	check_ajax_referer( 'my-special-string', 'security' );

	$s         = $_POST['s'];
	$post_type = $_POST['postType'];

	$args = array(

		'post_status'    => 'publish',
		'post_type'      => $post_type,
		'posts_per_page' => -1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		's'              => $s,

	);

	$output = array();

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) :

		while ( $the_query->have_posts() ) :
			$the_query->the_post();

			$post_title = get_the_title();
			$store_id   = get_field( 'store_id' );

			if ( ! in_array_r( $post_title, $output ) ) :

				array_push(
					$output, array(
						'title'    => $post_title,
						'store_id' => $store_id,
					)
				);

			endif;

		endwhile;

	endif;

	echo wp_json_encode( $output );

	wp_reset_postdata();

	exit();

}

add_action( 'wp_ajax_liveSearch', 'liveSearch' );
add_action( 'wp_ajax_nopriv_liveSearch', 'liveSearch' );

function search_store() {
	check_ajax_referer( 'my-special-string', 'security' );

	$s   = $_POST['s'];
	$key = $_POST['key'];

	$args = array(

		'post_status'    => 'publish',
		'post_type'      => 'stores',
		'posts_per_page' => 1,
		'orderby'        => 'title',
		'order'          => 'ASC',

	);

	if ( 'title' === $key ) :

		$args['s'] = $s;

	else :

		$args['meta_key']   = $meta_key;
		$args['meta_value'] = $s;

	endif;

	ob_start();

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) :

		while ( $the_query->have_posts() ) :
			$the_query->the_post();

			get_template_part( 'loop-templates/content', 'store-info' );

		endwhile;

	endif;

	$output = ob_get_contents();

	ob_end_clean();

	echo $output;

	wp_reset_postdata();

	exit();

}

add_action( 'wp_ajax_searchStore', 'searchStore' );
add_action( 'wp_ajax_nopriv_searchStore', 'searchStore' );
