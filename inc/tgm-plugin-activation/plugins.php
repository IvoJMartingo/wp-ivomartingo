<?php

/** Include the TGM_Plugin_Activation class. */
require_once get_template_directory() . '/inc/tgm-plugin-activation/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

function my_theme_register_required_plugins() {

	$plugins = array(
		array(
			'name'     => 'Advanced Custom Fields PRO',
			'slug'     => 'advanced-custom-fields-pro',
			'required' => true,
		),
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => true,
		),
		array(
			'name'     => 'Disable Comments',
			'slug'     => 'disable-comments',
			'required' => true,
		),
		array(
			'name'       => 'Easy WP SMTP',
			'slug'       => 'easy-wp-smtp',
			'recomended' => false,
		),
		array(
			'name'     => 'Flamingo',
			'slug'     => 'flamingo',
			'required' => true,
		),
		array(
			'name'     => 'Imagify – WebP & Image Compression',
			'slug'     => 'imagify',
			'recomended' => false,
		),
		array(
			'name'       => 'WP Super Cache',
			'slug'       => 'wp-super-cache',
			'recomended' => false,
		),
		array(
			'name'     => 'WP Migrate DB',
			'slug'     => 'wp-migrate-db',
			'recommended' => true,
		),
		array(
			'name'     => 'WPS Hide Login',
			'slug'     => 'wps-hide-login',
			'required' => false,
		),
		array(
			'name'        => 'Wordfence',
			'slug'        => 'wordfence',
			'recommended' => false,
		),
		array(
			'name'     => 'Yoast SEO',
			'slug'     => 'wordpress-seo',
			'required' => false,
		),
	);

	// Change this to your theme text domain, used for internationalising strings

	$config = array(
		'domain'       => 'dev-ibm',                   // Text domain - likely want to be the same as your theme.
		'default_path' => '',                          // Default absolute path to pre-packaged plugins
		'menu'         => 'install-required-plugins',  // Menu slug
		'has_notices'  => true,                        // Show admin notices or not
		'is_automatic' => true,                        // Automatically activate plugins after installation or not
		'message'      => '',                          // Message to output right before the plugins table
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'understrap' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'understrap' ),
			// translators: %1$s = plugin name
			'installing'                      => esc_html__( 'Installing Plugin: %s', 'understrap' ),
			'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'understrap' ),
			// translators: %1$s = plugin name
			'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'understrap' ),
			'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'understrap' ),
			'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'understrap' ),
			'return'                          => esc_html__( 'Return to Required Plugins Installer', 'understrap' ),
			'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'understrap' ),
			// translators: %1$s = dashboard link
			'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'understrap' ),
			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated' or 'error'
		),
	);

	tgmpa( $plugins, $config );
}
