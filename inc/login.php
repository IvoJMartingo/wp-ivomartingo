<?php
/**
 * Understrap login modify
 *
 * @package ivomartingo
 */
 
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
 
/**
 * Custom login logo
 */
function loginlogo_img() {
	echo '<style type="text/css">h1 a {background-image: url(' . esc_url( get_bloginfo( 'template_directory' ) ) . '/img/login-logo.png ) !important;width:320px!important;height:60px!important;background-size: 100%!important;}</style>';
}
//add_action( 'login_head', 'loginlogo_img' );

/**
 * Custom login url
 */
function loginlogo_url() {
	return 'http://ivomartingo.pt/';
}
add_action( 'login_headerurl', 'loginlogo_url' );

/**
 * Custom login title
 */
function loginlogo_title() {
	return 'Ivo Martingo';
}
add_action( 'login_headertext', 'loginlogo_title' );