<?php
/**
 * Understrap enqueue scripts
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'ibm-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $css_version );

		$filepath = '/css/bower_components/bower.min.css';

		if ( validate_file( $filepath ) < 1 && file_exists( get_template_directory() . $filepath ) ) {
			$filepath_version = $theme_version . '.' . filemtime( get_template_directory() . $filepath );
			wp_enqueue_style( 'bower-styles', get_template_directory_uri() . $filepath, array(), $filepath_version );
		}
		
		$filepath = '/js/bower_components/bower.min.js';

		if ( validate_file( $filepath ) < 1 && file_exists( get_template_directory() . $filepath ) ) {
			$filepath_version = $theme_version . '.' . filemtime( get_template_directory() . $filepath );
			wp_enqueue_script( 'bower-scripts', get_template_directory_uri() . $filepath, array(), $filepath_version, true );
		}
		
		$filepath = '/js/vendor.min.js';

		if ( validate_file( $filepath ) < 1 && file_exists( get_template_directory() . $filepath ) ) {
			$filepath_version = $theme_version . '.' . filemtime( get_template_directory() . $filepath );
			wp_enqueue_script( 'vendor-scripts', get_template_directory_uri() . $filepath, array(), $filepath_version, true );
		}

		$filepath = WP_ENV === 'production' ? '/js/theme.min.js' : '/js/theme.js';

		$filepath_version = $theme_version . '.' . filemtime( get_template_directory() . $filepath );
		wp_enqueue_script( 'ibm-scripts', get_template_directory_uri() . $filepath, array(), $filepath_version, true );

		/*
		Example single js
		if ( validate_file( $filepath ) < 1 && file_exists( get_template_directory() . $filepath ) ) {

			if ( function_exists( 'env' ) ){
				$filepath = env( 'WP_ENV' ) === 'development' ? '/js/single/simulador.js' : '/js/single/simulador.min.js';
			} else {
				$filepath = '/js/single/simulador.min.js';
			}

			$filepath_version = $theme_version . '.' . filemtime( get_template_directory() . $filepath );
			wp_enqueue_script( 'camp_poup-scripts', get_template_directory_uri() . $filepath, array(), $filepath_version, true );
		}
		*/

		wp_localize_script(
			'ibm-scripts', 'settings',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'siteurl' => site_url(),
				'dir' 	  => get_template_directory_uri(),
				'wpenv'   => WP_ENV,
			)
		);

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'understrap_scripts' ).


/**
 * Marketing Scripts to be add if options field are set.
 */
function marketing_scripts() {
	$gtag = get_option( 'gtag' );
	if ( $gtag && 'production' === WP_ENV ) :
		$goptimize = get_option( 'goptimize' );

		if ( $goptimize ) :
		?>
		<!-- Anti-flicker snippet (recommended)  -->
		<style>.async-hide { opacity: 0 !important} </style>
		<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
		h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
		(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
		})(window,document.documentElement,'async-hide','dataLayer',4000,
		{'<?php echo esc_attr( $goptimize ); ?>':true});</script>
		<?php endif; ?>
		<!-- Modified Analytics tracking code with Optimize plugin -->
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		window.onload = function() { 
			ga('create', '<?php echo esc_attr( $gtag ); ?>', 'auto');
			<?php if ( $goptimize ) : ?>
				ga('require', '<?php echo esc_attr( $goptimize ); ?>');
			<?php endif; ?>
			ga('send', 'pageview');
		};
		</script>
	<?php
	endif;

	$gtag = get_option( 'gtag_ads' );
	if ( $gtag && 'production' === WP_ENV ) :
	?>
		<!-- Global site tag (gtag.js) -->
		<script async src="https://www.googletagmanager.com/gtag/js?<?php echo esc_attr( $gtag ); ?>"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', '<?php echo esc_attr( $gtag ); ?>');
		</script>
	<?php
	endif;

	$hotjar = get_option( 'hotjar' ); 

	if ( $hotjar && 'production' === WP_ENV ) :
	?>
	<!-- Hotjar Tracking Code -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:<?php echo esc_attr( get_option( 'hotjar' ) );?>,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<?php
	endif;

	$fbpixel = get_option( 'fbpixel' );

	if ( $fbpixel && 'production' === WP_ENV ) :
	?>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '<?php echo esc_attr( get_option( 'fbpixel' ) ); ?>');
			fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo esc_attr( get_option( 'fbpixel' ) ); ?>&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
	<?php
	endif;
}
add_action( 'wp_head', 'marketing_scripts' );
