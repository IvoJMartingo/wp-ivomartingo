<?php

/**
 * Understrap forms functions
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

function form_register_checkin() {

	if ( wp_verify_nonce( $_POST['register_checkin_nonce'], 'checkin_nonce' ) ) {

		$username    = sanitize_text_field( $_POST['username'] );
		$useremail   = sanitize_email( $_POST['useremail'] );
		$fatura_id   = sanitize_text_field( $_POST['faturaID'] );
		$fatura_foto = $_FILES['faturaFoto'];
		$voucher_id  = uniqid( 'V' );
		$upload      = process_upload( $fatura_foto );

		if ( ! $upload ) {
			return false;
		}

		$post = wp_insert_post(
			array(
				'post_title'  => $voucher_id,
				'post_status' => 'pending',
				'post_type'   => 'checkin',
				'meta_input'  => array(
					'checkin_voucherID'   => $voucher_id,
					'checkin_award'       => 'teste',
					'checkin_awardID'     => 'dadas',
					'checkin_username'    => $username,
					'checkin_email'       => $useremail,
					'checkin_faturaID'    => $fatura_id,
					'checkin_fatura_foto' => $upload,
					'checkin_rest'        => 'czgq',
					'checkin_restNIF'     => '50101051548',
					'checkin_restID'      => 'gsdgsfd',
				),
			)
		);

	}

}

add_action( 'admin_post_nopriv_register_checkin', 'form_register_checkin' );
add_action( 'admin_post_register_checkin', 'form_register_checkin' );
