<?php

/**
 * ACF Blocks creator
 * 
 * for more array options: https://www.advancedcustomfields.com/resources/acf_register_block_type/
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


function register_acf_block_example() {
    // check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'example_name',
            'title'             => __('Example Block'),
            'description'       => __('A custom description.'),
            'render_template'   => 'template-parts/blocks/example/example.php', //template part path
            'category'          => 'formatting', // common / formatting / layout / widgets / embed
            'icon'              => 'admin-comments', // icon to identify the block

            //'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path fill="none" d="M0 0h24v24H0V0z" /><path d="M19 13H5v-2h14v2z" /></svg>', // icon to identify the block with svg

            // Specifying colors
            /* 'icon' => array(
                // Specifying a background color to appear with the icon e.g.: in the inserter.
                'background' => '#7e70af',
                // Specifying a color for the icon (optional: if not set, a readable color will be automatically defined)
                'foreground' => '#fff',
                // Specifying a dashicon for the block
                'src' => 'book-alt',
            ), */
            
            'keywords'          => array( 'example', 'block' ), // An array of search terms to help user discover the block while searching.
            //'post_types'        => array('post', 'page'), //An array of post types to restrict this block type to.
            'mode'              => 'preview', // auto, preview, edit
            'align'             => 'full', // “left”, “center”, “right”, “wide” and “full”
            //'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/example/example.css', //The url to a .css file to be enqueued whenever your block is displayed (front-end and back-end).
            //'enqueue_script'     => get_template_directory_uri() . '/template-parts/blocks/example/example.js',

            'support'           => array(  // An array of features to support
                //'align' => right, // disable alignment toolbar
                'align' => array( 'left', 'right', 'full' ), // customize alignment toolbar
                'mode' => true, // disable preview/edit toggle
                'multiple' => true, // this property allows the block to be added multiple times

            ), 

        ));

    }
}

// Check if function exists and hook into setup.
if( function_exists('register_acf_block_example') ) {
   // add_action('acf/init', 'register_acf_block_example');
}