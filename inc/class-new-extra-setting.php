<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Class for adding a new setting field
 *
 * @param array $arguments with the new setting fields.
 *
 * NOTE : define new seetings on theme-settings.php file
 *
 * Links :
 * https://developer.wordpress.org/reference/functions/register_setting/
 * https://codex.wordpress.org/Function_Reference/add_settings_section
 * https://shellcreeper.com/how-to-add-settings-section-in-wordpress-default-settings/
 *
 * Example:
 *
 * $address = new New_Extra_Setting( array(
 *   'id'          =>  'extra_blog_desc_id',
 *   'option_name' =>  'extra_blog_description',
 *   'title'       =>  '<label for="extra_blog_desc_id">' . __( 'Blog description' , 'extra_blog_description' ) . '</label>',
 *   'page'        =>  'general',
 *   'option_type' =>  'text'
 * ));
 *
 * Usage : get_option( 'extra_blog_description' );
 */

class New_Extra_Setting {

	public function __construct( array $arguments = array() ) {
		if ( ! empty( $arguments ) ) {
			foreach ( $arguments as $property => $argument ) {
				$this->{$property} = $argument;
			}

			add_action( 'admin_init', array( $this, 'register_fields' ) );
		}
	}

	/**
	 * Add new fields to wp-admin/options-general.php page
	 */
	public function register_fields() {
		register_setting( $this->page, $this->option_name, 'esc_attr' );
		add_settings_field(
			$this->id,
			$this->title,
			array( $this, 'fields_html' ),
			$this->page
		);
	}

	/**
	 * HTML for extra settings
	 */
	public function fields_html() {
		$value = get_option( $this->option_name, '' );

		switch ( $this->option_type ) {
			case 'textarea':
				echo '<textarea id="' . esc_attr( $this->id ) . '" name="' . esc_attr( $this->option_name ) . '" rows="4" cols="53">';
				echo esc_html( $value );
				echo '</textarea>';
				break;
			default:
				echo '<input type="' . esc_attr( $this->option_type ) . '" id="' . esc_attr( $this->id ) . '" name="' . esc_attr( $this->option_name ) . '" class="regular-text" value="' . esc_attr( $value ) . '" />';
				break;
		}

	}

}
