<?php
/**
 * understrap admin actions
 *
 * @package ivomartingo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Registers an admin stylesheet for the theme.
 */

add_action( 'admin_enqueue_scripts', 'understrap_admin_styles' );

if ( ! function_exists( 'understrap_admin_styles' ) ) {
	function understrap_admin_styles() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$filepath = '/css/admin.css';

		if ( validate_file( $filepath ) < 1 && file_exists( get_template_directory() . $filepath ) ) {
			$filepath_version = $theme_version . '.' . filemtime( get_template_directory() . $filepath );
			wp_enqueue_style( 'admin-css', get_stylesheet_directory_uri() . $filepath, array(), $filepath_version );
		}
	}
}

/**
 * Remove post's and comments from menu
 */

add_action( 'admin_menu', 'remove_from_menu' );

if ( ! function_exists( 'remove_from_menu' ) ) {
	function remove_from_menu() {
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'edit-comments.php' );
	}
}
