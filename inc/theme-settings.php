<?php
/**
 * Check and setup theme's default settings
 *
 * @package ivomartingo
 *
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'understrap_setup_theme_default_settings' ) ) {
	function understrap_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$understrap_posts_index_style = get_theme_mod( 'understrap_posts_index_style' );
		if ( '' == $understrap_posts_index_style ) {
			set_theme_mod( 'understrap_posts_index_style', 'default' );
		}

		// Sidebar position.
		$understrap_sidebar_position = get_theme_mod( 'understrap_sidebar_position' );
		if ( '' == $understrap_sidebar_position ) {
			set_theme_mod( 'understrap_sidebar_position', 'right' );
		}

		// Container width.
		$understrap_container_type = get_theme_mod( 'understrap_container_type' );
		if ( '' == $understrap_container_type ) {
			set_theme_mod( 'understrap_container_type', 'container' );
		}
	}
}

if ( class_exists( 'New_Extra_Setting' ) ) {
	new New_Extra_Setting( array(
		'id'          => 'gtag_id',
		'option_name' => 'gtag',
		'title'       => '<label for="gtag_id">' . __( 'Google Analytics ID', 'gtag' ) . '</label>',
		'page'        => 'general',
		'option_type' => 'text',
	));

	new New_Extra_Setting( array(
		'id'          => 'goptimize_id',
		'option_name' => 'goptimize',
		'title'       => '<label for="goptimize_id">' . __( 'Google Optimize Container', 'goptimize' ) . '</label>',
		'page'        => 'general',
		'option_type' => 'text',
	));

	new New_Extra_Setting( array(
		'id'          => 'gtag_ads_id',
		'option_name' => 'gtag_ads',
		'title'       => '<label for="gtag_ads_id">' . __( 'Google Ads ID', 'gtag_ads' ) . '</label>',
		'page'        => 'general',
		'option_type' => 'text',
	));

	new New_Extra_Setting( array(
		'id'          => 'gtag_lead_conversion_id',
		'option_name' => 'gtag_lead_conversion',
		'title'       => '<label for="gtag_lead_conversion_id">' . __( 'Google Lead Conversion ID', 'gtag_lead_conversion' ) . '</label>',
		'page'        => 'general',
		'option_type' => 'text',
	));

	new New_Extra_Setting( array(
		'id'          => 'fbpixel_id',
		'option_name' => 'fbpixel',
		'title'       => '<label for="fbpixel_id">' . __( 'Facebook Pixel ID', 'fbpixel' ) . '</label>',
		'page'        => 'general',
		'option_type' => 'text',
	));

	new New_Extra_Setting( array(
		'id'          => 'hotjar_id',
		'option_name' => 'hotjar',
		'title'       => '<label for="hotjar_id">' . __( 'Hotjar', 'hotjar' ) . '</label>',
		'page'        => 'general',
		'option_type' => 'text',
	));
}
