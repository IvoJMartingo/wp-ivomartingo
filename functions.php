<?php
/**
 * ccenergia_landings functions and definitions
 *
 * @package ccenergia_landings
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$ccenergia_landings_includes = array(
	'/admin.php',                           // Load Admin hooks and actions
	'/class-new-extra-setting.php',         // New settings to options-page class
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/class-custom-widget.php',             // Custom widget class.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/login.php',                           // Custom login settings.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/editor.php',                          // Load Editor functions.
	'/register-post-type.php',              // Load post type register functions.
	'/register-taxonomy.php',               // Load taxonomy register functions.
	'/wp-admin.php',                        // /wp-admin/ related functions	
	'/tgm-plugin-activation/plugins.php',   // Load TGM Plugin activation
	'/ajax.php',                            // Load Ajax functions.
	//'/image-functions.php',                 // Load Image functions.
	//'/acf-blocks.php',                      // Load ACF Blocks functions.
	//'/process-upload.php',                  // Custom process upload WordPress
	//'/wp-mail.php',                         // Custom mail functions.
	//'/forms.php',                           // Custom forms functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ( $ccenergia_landings_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
